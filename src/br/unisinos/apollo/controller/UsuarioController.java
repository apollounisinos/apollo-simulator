package br.unisinos.apollo.controller;

import br.unisinos.apollo.model.bean.Usuario;
import java.util.Date;

/**
 * Created 07/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class UsuarioController {

    public Double getIMC(Usuario user) {
        //Calc the IMC
        Double imc = user.getPeso() / (user.getAltura() * user.getAltura());
        //Return it
        return imc;
    }

    public Integer getIdade(Usuario user) {
        //Calc the age
        Date now = new Date();
        Integer age = now.getYear() - user.getDataNascimento().getYear();
        //Return it
        return age;
    }

}
