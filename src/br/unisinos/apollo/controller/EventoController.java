package br.unisinos.apollo.controller;

import br.unisinos.apollo.model.bean.EventoUsuario;
import br.unisinos.apollo.model.dao.ConnectionDAO;
import br.unisinos.apollo.model.dao.EventoDAO;
import br.unisinos.apollo.model.manager.EventoManager;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created 05/02/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class EventoController {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");

    public void exportarEvento(Integer userID) {
        try {
            //Begin connection
            Connection connection = new ConnectionDAO().connect();
            //Retrieve the events
            List<EventoUsuario> data = new EventoDAO().getEvento(connection, userID);
            //Export it to disk
            new EventoManager().exportCSV(data, "Evento_" + sdf.format(new Date()) + ".csv");
            //Close connection
            new ConnectionDAO().disconnect(connection);
        } catch (Exception e) {
            System.out.println("Fail exporting the event");
        }
    }

}
