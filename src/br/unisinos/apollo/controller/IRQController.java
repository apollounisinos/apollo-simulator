package br.unisinos.apollo.controller;

import br.unisinos.apollo.model.bean.Extrinseco;
import br.unisinos.apollo.model.bean.ExtrinsecoPond;
import br.unisinos.apollo.model.bean.Intrinseco;
import br.unisinos.apollo.model.bean.IntrinsecoPond;
import br.unisinos.apollo.model.bean.MonitoramentoAmbiente;
import br.unisinos.apollo.model.bean.FatorPond;
import br.unisinos.apollo.model.bean.Usuario;
import br.unisinos.apollo.model.dao.ExtrinsecoDAO;
import br.unisinos.apollo.model.dao.GeralDAO;
import br.unisinos.apollo.model.dao.IMCPondDAO;
import br.unisinos.apollo.model.dao.IdadePondDAO;
import br.unisinos.apollo.model.dao.IntrinsecoDAO;
import br.unisinos.apollo.model.dao.LuzPondDAO;
import br.unisinos.apollo.model.dao.SexoPondDAO;
import br.unisinos.apollo.model.dao.TemperaturaPondDAO;
import br.unisinos.apollo.model.dao.UsuarioDAO;
import br.unisinos.apollo.model.util.FormatUtil;
import br.unisinos.apollo.view.Main;
import java.sql.Connection;

/**
 * Created 10/01/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class IRQController {

    private final FormatUtil fUtil = new FormatUtil();
    private final Integer limiar = 6;
    private double fatorIntrinseco = 0.0;
    private double fatorExtrinseco = 0.0;
    private double fatorUsuario = 0.0;
    private FatorPond pond = new FatorPond();

    public IRQController(Connection connection) {
        pond = new GeralDAO().getFatorPond(connection);
    }

    public void calcularIRQInicial(Connection connection, Integer userID) {
        //Get intrinseco
        fatorIntrinseco = this.getFatorIntrinseco(connection, userID);
        //Get extrinseco
        fatorExtrinseco = this.getFatorExtrinseco(connection, userID);
        //Get fator usuario
        fatorUsuario = this.getFatorUsuario(connection, userID);
    }

    public double calcularIRQ(Connection connection, Integer userID, MonitoramentoAmbiente ma) {
        //Get intrinseco
        Main.appendText(fUtil.formatDoubleValue(fatorIntrinseco) + "\t", false);
        //Get extrinseco
        Main.appendText(fUtil.formatDoubleValue(fatorExtrinseco) + "\t", false);
        //Get fator usuario
        Main.appendText(fUtil.formatDoubleValue(fatorUsuario) + "\t", false);
        //Get fator ambiente
        double fatorAmbiente = this.getFatorAmbiente(connection, ma);
        Main.appendText(fUtil.formatDoubleValue(fatorAmbiente) + "\t", false);

        //Divide
        double irqScore = ((fatorIntrinseco * pond.getFi())
                + (fatorExtrinseco * pond.getFe())
                + (fatorUsuario * pond.getFu())
                + (fatorAmbiente * pond.getFa()))
                / (4 * pond.getTotal());
        return limiar - (irqScore * 0.01);
    }

    private double getFatorIntrinseco(Connection connection, Integer id) {
        Intrinseco in = new IntrinsecoDAO().getIntrinseco(connection, id);
        IntrinsecoPond inPeso = new IntrinsecoDAO().getIntrinsecoPond(connection);
        int score = 0;

        if (in.getAntecedenteQueda()) {
            score += inPeso.getAntecedenteQueda();
        }

        if (in.getDeficitEquilibrio()) {
            score += inPeso.getDeficitEquilibrio();
        }

        if (in.getDeficitVisual()) {
            score += inPeso.getDeficitVisual();
        }

        if (in.getDeficitMarcha()) {
            score += inPeso.getDeficitMarcha();
        }

        if (in.getDemencia()) {
            score += inPeso.getDemencia();
        }

        if (in.getMedoCair()) {
            score += inPeso.getMedoCair();
        }

        if (in.getFraquezaMuscular()) {
            score += inPeso.getFraquezaMuscular();
        }

        if (in.getDeficienciaAuditiva()) {
            score += inPeso.getDeficienciaAuditiva();
        }

        if (in.getSensibilidade()) {
            score += inPeso.getSensibilidade();
        }

        if (in.getDeficienciaFisica()) {
            score += inPeso.getDeficienciaFisica();
        }

        if (in.getOsteoporose()) {
            score += inPeso.getOsteoporose();
        }

        if (in.getArtrite()) {
            score += inPeso.getArtrite();
        }

        if (in.getDoencaReumatica()) {
            score += inPeso.getDoencaReumatica();
        }

        if (in.getSincope()) {
            score += inPeso.getSincope();
        }

        if (in.getLesaoSistemaNervosoCentral()) {
            score += inPeso.getLesaoSistemaNervosoCentral();
        }

        if (in.getProblemasNoPe()) {
            score += inPeso.getProblemasNoPe();
        }

        if (in.getDepressao()) {
            score += inPeso.getDepressao();
        }

        if (in.getParkinson()) {
            score += inPeso.getParkinson();
        }

        if (in.getVertigem()) {
            score += inPeso.getVertigem();
        }

        if (in.getDiabetes()) {
            score += inPeso.getDiabetes();
        }

        if (in.getAlteracaoPostural()) {
            score += inPeso.getAlteracaoPostural();
        }

        if (in.getAntecedenteAVC()) {
            score += inPeso.getAntecedenteAVC();
        }

        if (in.getIncontinenciaUrinaria()) {
            score += inPeso.getIncontinenciaUrinaria();
        }

        if (in.getComorbidade()) {
            score += inPeso.getComorbidade();
        }

        if (in.getAutoPercepcaoEstadoSaude()) {
            score += inPeso.getAutoPercepcaoEstadoSaude();
        }

        if (in.getDor()) {
            score += inPeso.getDor();
        }
        //Percent
        double div = (double) score / (double) inPeso.sum();
        div = div * 100.0;
        return div;
    }

    private double getFatorExtrinseco(Connection connection, Integer id) {
        Extrinseco ex = new ExtrinsecoDAO().getExtrinseco(connection, id);
        ExtrinsecoPond exPeso = new ExtrinsecoDAO().getExtrinsecoPond(connection);
        int score = 0;

        //===========================================================
        //======================= Extrinseco ========================
        //===========================================================s
        if (ex.getCompromentimentoMobilidade()) {
            score += exPeso.getCompromentimentoMobilidade();
        }

        if (ex.getRiscoAmbiental()) {
            score += exPeso.getRiscoAmbiental();
        }

        if (ex.getInternLongaPermanencia()) {
            score += exPeso.getInternLongaPermanencia();
        }

        if (ex.getViveSozinho()) {
            score += exPeso.getViveSozinho();
        }

        if (ex.getReducaoAtividadesBasicas()) {
            score += exPeso.getReducaoAtividadesBasicas();
        }

        if (ex.getCapacidadeManterseEmPe()) {
            score += exPeso.getCapacidadeManterseEmPe();
        }

        if (ex.getCapacidadeTransferirse()) {
            score += exPeso.getCapacidadeTransferirse();
        }

        if (ex.getCapacidadeDeitarLevantar()) {
            score += exPeso.getCapacidadeDeitarLevantar();
        }

        if (ex.getDependenciaFuncional()) {
            score += exPeso.getDependenciaFuncional();
        }

        if (ex.getSedentarismo()) {
            score += exPeso.getSedentarismo();
        }

        if (ex.getPolifarmacia()) {
            score += exPeso.getPolifarmacia();
        }

        if (ex.getUsoDrogasPsicoativas()) {
            score += exPeso.getUsoDrogasPsicoativas();
        }

        if (ex.getBenzoDiazepinicos()) {
            score += exPeso.getBenzoDiazepinicos();
        }

        if (ex.getAntiPsicoticos()) {
            score += exPeso.getAntiPsicoticos();
        }

        if (ex.getAntiDepressivos()) {
            score += exPeso.getAntiDepressivos();
        }

        if (ex.getSedativos()) {
            score += exPeso.getSedativos();
        }

        if (ex.getAnsioliticos()) {
            score += exPeso.getAnsioliticos();
        }

        if (ex.getHipnoticos()) {
            score += exPeso.getHipnoticos();
        }

        if (ex.getFenotiazina()) {
            score += exPeso.getFenotiazina();
        }

        if (ex.getAntiHipertensivos()) {
            score += exPeso.getAntiHipertensivos();
        }

        if (ex.getAntiEpileticos()) {
            score += exPeso.getAntiEpileticos();
        }

        if (ex.getConsumoAlcool()) {
            score += exPeso.getConsumoAlcool();
        }

        //Percent
        double div = (double) score / (double) exPeso.sum();
        div = div * 100.0;
        return div;
    }

    private double getFatorUsuario(Connection connection, Integer id) {
        UsuarioController userController = new UsuarioController();
        Usuario user = new UsuarioDAO().getInformacaoUsuario(connection, id);
        //Sum the score
        Double imc = userController.getIMC(user);
        Integer imcPond = new IMCPondDAO().getPond(connection, imc);
        Integer age = userController.getIdade(user);
        Integer agePond = new IdadePondDAO().getPond(connection, age);
        Integer sexoPond = new SexoPondDAO().getPond(connection, user.getSexo());
        double fu = (((double) imcPond + (double) agePond + (double) sexoPond) / 30) * 100;
        return fu;
    }

    private double getFatorAmbiente(Connection connection, MonitoramentoAmbiente ma) {
        Integer temperature = new TemperaturaPondDAO().getPond(connection, ma.getTemperatura());
        Integer light = new LuzPondDAO().getPond(connection, ma.getIluminacao().intValue());
        double fu = (((double) temperature + (double) light) / 20) * 100;
        return fu;
    }

    public static void main(String[] args) {
        //System.out.println(new IRQController().calcularIRQInicial(2));
    }

}
