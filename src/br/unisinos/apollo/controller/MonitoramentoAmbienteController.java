package br.unisinos.apollo.controller;

import br.unisinos.apollo.model.bean.MonitoramentoAmbiente;
import br.unisinos.apollo.model.dao.ConnectionDAO;
import br.unisinos.apollo.model.dao.MonitoramentoAmbienteDAO;
import java.sql.Connection;

/**
 * Created 27/05/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com] =D
 */
public class MonitoramentoAmbienteController {

    public void persistirMonitoramentoAmbiente(MonitoramentoAmbiente monitor) {
        Connection connection = new ConnectionDAO().connect();
        new MonitoramentoAmbienteDAO().persistMonitoramentoAmbiente(connection, monitor);
        new ConnectionDAO().disconnect(connection);
    }

}
