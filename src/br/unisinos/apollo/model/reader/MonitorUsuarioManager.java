package br.unisinos.apollo.model.reader;

import br.unisinos.apollo.model.bean.MovimentoUsuario;
import br.unisinos.apollo.model.util.ValidationUtil;
import br.unisinos.apollo.model.util.WorkbookUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

/**
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class MonitorUsuarioManager {

    public List<MovimentoUsuario> read(String path) {
        WorkbookUtil wUtil = new WorkbookUtil();
        ValidationUtil vUtil = new ValidationUtil();
        XSSFSheet sheet = wUtil.getSheet(wUtil.openWorkbook(path), 0);
        //Leitura
        List<MovimentoUsuario> data = readWorkbook(sheet, vUtil.retrieveLastIndexRow(sheet));
        return data;
    }

    private List<MovimentoUsuario> readWorkbook(XSSFSheet sheet, Integer rowLimit) {
        WorkbookUtil wUtil = new WorkbookUtil();
        List<MovimentoUsuario> data = new ArrayList<>();
        try {
            for (int r = 1; r < rowLimit; r++) {
                MovimentoUsuario mu = new MovimentoUsuario();
                Row row = sheet.getRow(r);
                //movimentousuario_id
                mu.setMonitoramentoUsuarioID(Integer.parseInt(wUtil.readString(row, 0)));
                //usuario_id
                mu.setUsuarioID(Integer.parseInt(wUtil.readString(row, 1)));
                //datahora
                mu.setDatahora(wUtil.readDate(row, 2));
                //indoor
//                mu.setIndoor(wUtil.readString(row, 3));
                //latitude
//                mu.setLatitude(wUtil.readString(row, 4));
                //longitude
//                mu.setLongitude(wUtil.readString(row, 5));
                //aceleracao
                mu.setModuloAceleracao(wUtil.readDouble(row, 6));
                //aceleracao_x
                mu.setxAceleracao(wUtil.readDouble(row, 7));
                //aceleracao_y
                mu.setyAceleracao(wUtil.readDouble(row, 8));
                //aceleracao_z
                mu.setzAceleracao(wUtil.readDouble(row, 9));
                //inclinacao_x
                mu.setxInclinacao(wUtil.readDouble(row, 10));
                //inclinacao_y
                mu.setyInclinacao(wUtil.readDouble(row, 11));
                //inclinacao_z
                mu.setzInclinacao(wUtil.readDouble(row, 12));

                data.add(mu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Lidos: " + data.size());
        return data;
    }

}
