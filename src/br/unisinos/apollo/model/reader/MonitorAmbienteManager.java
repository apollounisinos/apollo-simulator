package br.unisinos.apollo.model.reader;

import br.unisinos.apollo.model.bean.MonitoramentoAmbiente;
import br.unisinos.apollo.model.util.ValidationUtil;
import br.unisinos.apollo.model.util.WorkbookUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

/**
 * Created 16/01/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class MonitorAmbienteManager {

    public List<MonitoramentoAmbiente> read(String path) {
        WorkbookUtil wUtil = new WorkbookUtil();
        ValidationUtil vUtil = new ValidationUtil();
        XSSFSheet sheet = wUtil.getSheet(wUtil.openWorkbook(path), 0);
        //Leitura
        List<MonitoramentoAmbiente> data = readWorkbook(sheet, vUtil.retrieveLastIndexRow(sheet));
        return data;
    }

    private List<MonitoramentoAmbiente> readWorkbook(XSSFSheet sheet, Integer rowLimit) {
        WorkbookUtil wUtil = new WorkbookUtil();
        List<MonitoramentoAmbiente> data = new ArrayList<>();
        try {
            for (int r = 1; r < rowLimit; r++) {
                MonitoramentoAmbiente mu = new MonitoramentoAmbiente();
                Row row = sheet.getRow(r);
                //monitoramentoambiente_id
                mu.setMonitoramentoAmbienteID(Integer.parseInt(wUtil.readString(row, 0)));
                //usuario_id
                mu.setAmbienteID(Integer.parseInt(wUtil.readString(row, 1)));
                //temperaturaC
                mu.setTemperatura(Integer.parseInt(wUtil.readString(row, 2)));
                //iluminacao
                mu.setIluminacao(wUtil.readDouble(row, 3));
                //datahora
                mu.setDatahora(wUtil.readDate(row, 4));
                data.add(mu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Lidos: " + data.size());
        return data;
    }

}
