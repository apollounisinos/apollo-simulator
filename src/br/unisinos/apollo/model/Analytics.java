package br.unisinos.apollo.model;

import br.unisinos.apollo.controller.IRQController;
import br.unisinos.apollo.model.bean.EventoUsuario;
import br.unisinos.apollo.model.bean.MonitoramentoAmbiente;
import br.unisinos.apollo.model.bean.MovimentoUsuario;
import br.unisinos.apollo.model.dao.ConnectionDAO;
import br.unisinos.apollo.model.dao.EventoDAO;
import br.unisinos.apollo.model.dao.GeralDAO;
import br.unisinos.apollo.model.dao.LuzPondDAO;
import br.unisinos.apollo.model.reader.MonitorAmbienteManager;
import br.unisinos.apollo.model.reader.MonitorUsuarioManager;
import br.unisinos.apollo.model.util.FormatUtil;
import br.unisinos.apollo.view.Main;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created 10/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com] =D
 */
public class Analytics {

    private SimpleDateFormat datetime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private final FormatUtil fUtil = new FormatUtil();

    public void analysis(Main main, String pathDatasetUsuario, String pathDatasetAmbiente, Double range1, Double range2, Double range3, Integer userID) {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                main.disableComponents();
                Connection connection = new ConnectionDAO().connect();
                //Clear evento table
                new GeralDAO().limparEvento(connection);
                //Select all the data
                List<MovimentoUsuario> dataUser = new MonitorUsuarioManager().read(pathDatasetUsuario);
                List<MonitoramentoAmbiente> dataEnv = new MonitorAmbienteManager().read(pathDatasetAmbiente);
                //Analyse item by item
                Main.appendText("Processamento iniciado", true);
                IRQController irqController = new IRQController(connection);
                irqController.calcularIRQInicial(connection, userID);
                int lastIluminacao = 3;
                for (int i = 0; i < dataUser.size(); i++) {
                    Main.appendText(datetime.format(new Date()) + "\t", false);
                    Main.appendText(i + "\t", false);
                    //Find the specific MonitoramentoAmbiente
                    MonitoramentoAmbiente ma = retrieveMonitorAmbiente(dataUser.get(i), dataEnv);
                    Double limiar = irqController.calcularIRQ(connection, userID, ma);
                    Main.appendText(fUtil.formatDoubleValue(limiar) + "\t", false);
                    if (checkModule(dataUser.get(i), limiar - 4.0, limiar - 2.0)) {
                        Main.appendText(fUtil.formatDoubleValue(dataUser.get(i).getModuloAceleracao()) + "\t", false);
                        //Retrieve the interval
                        List<MovimentoUsuario> cutted = retrieveCut(dataUser, i);
                        //Calculate the inclination Y media
                        Double mediumY = mediumY(cutted);
                        //If the medium bigger than 10°, save the data on Evento table
                        Main.appendText(fUtil.formatDoubleValue(mediumY) + "\t", false);
                        if (Math.abs(mediumY) > range1) {
                            EventoUsuario event = new EventoUsuario();
                            event.setUsuarioID(userID);
                            event.setTipo("1");
                            event.setDatahora(dataUser.get(i).getDatahora());
                            event.setMovimentoUsuarioID(dataUser.get(i).getMonitoramentoUsuarioID() + "");
                            new EventoDAO().persistirEvento(connection, event);
                            System.out.println("Persisted!");
                            Main.appendText("1\t", false);
                            i = i + 200;
                        }
                    } else if (checkModule(dataUser.get(i), limiar - 2.0, limiar)) {
                        Main.appendText(fUtil.formatDoubleValue(dataUser.get(i).getModuloAceleracao()) + "\t", false);
                        //Retrieve the interval
                        List<MovimentoUsuario> cutted = retrieveCut(dataUser, i);
                        //Calculate the inclination Y media
                        Double mediumY = mediumY(cutted);
                        //If the medium bigger than 20°, save the data on Evento table
                        Main.appendText(fUtil.formatDoubleValue(mediumY) + "\t", false);
                        if (Math.abs(mediumY) > range2) {
                            EventoUsuario event = new EventoUsuario();
                            event.setUsuarioID(userID);
                            event.setTipo("2");
                            event.setDatahora(dataUser.get(i).getDatahora());
                            event.setMovimentoUsuarioID(dataUser.get(i).getMonitoramentoUsuarioID() + "");
                            new EventoDAO().persistirEvento(connection, event);
                            System.out.println("Persisted!");
                            Main.appendText("2\t", false);
                            i = i + 200;
                        }
                    } else if (checkModule(dataUser.get(i), limiar, 1000.0)) {
                        Main.appendText(fUtil.formatDoubleValue(dataUser.get(i).getModuloAceleracao()) + "\t", false);
                        //Retrieve the interval
                        List<MovimentoUsuario> cutted = retrieveCut(dataUser, i);
                        //Calculate the inclination Y media
                        Double mediumY = mediumY(cutted);
                        //If the medium bigger than 45°, save the data on Evento table
                        Main.appendText(fUtil.formatDoubleValue(mediumY) + "\t", false);
                        if (Math.abs(mediumY) > range3) {
                            EventoUsuario event = new EventoUsuario();
                            event.setUsuarioID(userID);
                            event.setTipo("3");
                            event.setDatahora(dataUser.get(i).getDatahora());
                            event.setMovimentoUsuarioID(dataUser.get(i).getMonitoramentoUsuarioID() + "");
                            new EventoDAO().persistirEvento(connection, event);
                            System.out.println("Persisted!");
                            Main.appendText("3\t", false);
                            i = i + 200;
                        }
                    }
                    // ================== AMBIENTE ===================
                    int iluminacao = new LuzPondDAO().getStatus(connection, ma.getIluminacao().intValue());
                    if (lastIluminacao != iluminacao) {
                        EventoUsuario event = new EventoUsuario();
                        event.setUsuarioID(userID);
                        System.out.println("iluminacao " + iluminacao);
                        if (iluminacao == 0) {
                            event.setTipo("5");
                        }
                        if (iluminacao == 1) {
                            event.setTipo("6");
                        }
                        event.setDatahora(dataUser.get(i).getDatahora());
                        event.setMovimentoUsuarioID(dataUser.get(i).getMonitoramentoUsuarioID() + "");
                        new EventoDAO().persistirEvento(connection, event);
                        //Set last
                        lastIluminacao = iluminacao;
                    }

                    Main.appendText("", true);
                }
                Main.appendText("Processamento finalizado", true);
                new ConnectionDAO().disconnect(connection);
                main.enableComponents();
            }
        });
        t1.start();
    }

    private boolean checkModule(MovimentoUsuario mov, Double limiarMin, Double limiarMax) {
        if (mov.getModuloAceleracao() > limiarMin && mov.getModuloAceleracao() <= limiarMax) {
            return true;
        } else {
            return false;
        }
    }

    private List<MovimentoUsuario> retrieveCut(List<MovimentoUsuario> data, int index) {
        int range = 150;
        ArrayList<MovimentoUsuario> cutted = new ArrayList<>();

        if (range > index && data.size() > (index + range)) {
            cutted.addAll(data.subList(0, index + range));

        } else if (range > index && (data.size() - 1) < (index + range)) {
            cutted.addAll(data.subList(0, (data.size() - 1)));

        } else if (range < index && data.size() > (index + range)) {
            cutted.addAll(data.subList(index - range, index + range));

        } else if (range < index && data.size() < (index + range)) {
            cutted.addAll(data.subList(index - range, (data.size() - 1)));
        }
        return cutted;
    }

    private Double mediumY(List<MovimentoUsuario> data) {
        Double sum = 0.0;
        for (int i = 0; i < data.size(); i++) {
            sum = sum + data.get(i).getyInclinacao();
        }
        return sum / data.size();
    }

    private MonitoramentoAmbiente retrieveMonitorAmbiente(MovimentoUsuario mu, List<MonitoramentoAmbiente> maList) {
        SimpleDateFormat dateTime = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        MonitoramentoAmbiente ma = new MonitoramentoAmbiente();
        String actualDate = dateTime.format(mu.getDatahora());
        for (int i = 0; i < maList.size(); i++) {
            String envDate = dateTime.format(maList.get(i).getDatahora());
            if (actualDate.equalsIgnoreCase(envDate)) {
                ma = maList.get(i);
            }
        }
        return ma;
    }

}
