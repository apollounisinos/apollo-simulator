package br.unisinos.apollo.model.manager;

import br.unisinos.apollo.model.bean.EventoUsuario;
import br.unisinos.apollo.model.util.FileUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.List;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class EventoManager {

    public boolean writeWorkbook(List<EventoUsuario> data, String fileOutPath) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        XSSFWorkbook work = new XSSFWorkbook();
        XSSFSheet sheet = work.createSheet("Evento");
        XSSFCell cell;
        XSSFRow row = sheet.createRow(sheet.getPhysicalNumberOfRows());

        String[] headers = {"Event ID", "User ID", "Mov ID", "Event Date", "Event Type"};

        for (int x = 0; x < headers.length; x++) {
            cell = row.createCell(x);
            cell.setCellValue(headers[x]);
        }

        for (int x = 0; x < data.size(); x++) {
            int cont = 0;
            row = sheet.createRow(sheet.getPhysicalNumberOfRows());
            //Event ID
            cell = row.createCell(cont++);
            cell.setCellValue(data.get(x).getEventoID());
            //User ID
            cell = row.createCell(cont++);
            cell.setCellValue(data.get(x).getUsuarioID());
            //Mov ID
            cell = row.createCell(cont++);
            cell.setCellValue(data.get(x).getMovimentoUsuarioID());
            //DateTime
            cell = row.createCell(cont++);
            cell.setCellValue(dateFormat.format(data.get(x).getDatahora()));
            //Event type
            cell = row.createCell(cont++);
            cell.setCellValue(data.get(x).getTipo());
        }
        try {
            try (FileOutputStream out = new FileOutputStream(new File(fileOutPath))) {
                work.write(out);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Export data as id;user id; mov ou amb id; datetime event; event type
     * [1,2,3,6,7]
     *
     * @param data
     * @param fileOutPath
     */
    public void exportCSV(List<EventoUsuario> data, String fileOutPath) {
        SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        FileUtil fUtil = new FileUtil();
        for (int i = 0; i < data.size(); i++) {
            String csv = "";
            csv += data.get(i).getEventoID() + ";";
            csv += data.get(i).getUsuarioID()+ ";";
            csv += data.get(i).getMovimentoUsuarioID() + ";";
            csv += dateformat.format(data.get(i).getDatahora()) + ";";
            csv += data.get(i).getTipo() + ";";
            fUtil.saveLog(fileOutPath, csv, null, false);

        }
    }

}
