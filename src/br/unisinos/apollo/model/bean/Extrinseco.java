package br.unisinos.apollo.model.bean;

/**
 * Created 08/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class Extrinseco {

    private Boolean compromentimentoMobilidade;
    private Boolean riscoAmbiental;
    private Boolean internLongaPermanencia;
    private Boolean viveSozinho;
    private Boolean reducaoAtividadesBasicas;
    private Boolean capacidadeManterseEmPe;
    private Boolean capacidadeTransferirse;
    private Boolean capacidadeDeitarLevantar;
    private Boolean dependenciaFuncional;
    private Boolean sedentarismo;
    private Boolean polifarmacia;
    private Boolean usoDrogasPsicoativas;
    private Boolean benzoDiazepinicos;
    private Boolean antiPsicoticos;
    private Boolean antiDepressivos;
    private Boolean sedativos;
    private Boolean ansioliticos;
    private Boolean hipnoticos;
    private Boolean fenotiazina;
    private Boolean antiHipertensivos;
    private Boolean antiEpileticos;
    private Boolean consumoAlcool;

    public Boolean getCompromentimentoMobilidade() {
        return compromentimentoMobilidade;
    }

    public void setCompromentimentoMobilidade(Boolean compromentimentoMobilidade) {
        this.compromentimentoMobilidade = compromentimentoMobilidade;
    }

    public Boolean getRiscoAmbiental() {
        return riscoAmbiental;
    }

    public void setRiscoAmbiental(Boolean riscoAmbiental) {
        this.riscoAmbiental = riscoAmbiental;
    }

    public Boolean getInternLongaPermanencia() {
        return internLongaPermanencia;
    }

    public void setInternLongaPermanencia(Boolean internLongaPermanencia) {
        this.internLongaPermanencia = internLongaPermanencia;
    }

    public Boolean getViveSozinho() {
        return viveSozinho;
    }

    public void setViveSozinho(Boolean viveSozinho) {
        this.viveSozinho = viveSozinho;
    }

    public Boolean getReducaoAtividadesBasicas() {
        return reducaoAtividadesBasicas;
    }

    public void setReducaoAtividadesBasicas(Boolean reducaoAtividadesBasicas) {
        this.reducaoAtividadesBasicas = reducaoAtividadesBasicas;
    }

    public Boolean getCapacidadeManterseEmPe() {
        return capacidadeManterseEmPe;
    }

    public void setCapacidadeManterseEmPe(Boolean capacidadeManterseEmPe) {
        this.capacidadeManterseEmPe = capacidadeManterseEmPe;
    }

    public Boolean getCapacidadeTransferirse() {
        return capacidadeTransferirse;
    }

    public void setCapacidadeTransferirse(Boolean capacidadeTransferirse) {
        this.capacidadeTransferirse = capacidadeTransferirse;
    }

    public Boolean getCapacidadeDeitarLevantar() {
        return capacidadeDeitarLevantar;
    }

    public void setCapacidadeDeitarLevantar(Boolean capacidadeDeitarLevantar) {
        this.capacidadeDeitarLevantar = capacidadeDeitarLevantar;
    }

    public Boolean getDependenciaFuncional() {
        return dependenciaFuncional;
    }

    public void setDependenciaFuncional(Boolean dependenciaFuncional) {
        this.dependenciaFuncional = dependenciaFuncional;
    }

    public Boolean getSedentarismo() {
        return sedentarismo;
    }

    public void setSedentarismo(Boolean sedentarismo) {
        this.sedentarismo = sedentarismo;
    }

    public Boolean getPolifarmacia() {
        return polifarmacia;
    }

    public void setPolifarmacia(Boolean polifarmacia) {
        this.polifarmacia = polifarmacia;
    }

    public Boolean getUsoDrogasPsicoativas() {
        return usoDrogasPsicoativas;
    }

    public void setUsoDrogasPsicoativas(Boolean usoDrogasPsicoativas) {
        this.usoDrogasPsicoativas = usoDrogasPsicoativas;
    }

    public Boolean getBenzoDiazepinicos() {
        return benzoDiazepinicos;
    }

    public void setBenzoDiazepinicos(Boolean benzoDiazepinicos) {
        this.benzoDiazepinicos = benzoDiazepinicos;
    }

    public Boolean getAntiPsicoticos() {
        return antiPsicoticos;
    }

    public void setAntiPsicoticos(Boolean antiPsicoticos) {
        this.antiPsicoticos = antiPsicoticos;
    }

    public Boolean getAntiDepressivos() {
        return antiDepressivos;
    }

    public void setAntiDepressivos(Boolean antiDepressivos) {
        this.antiDepressivos = antiDepressivos;
    }

    public Boolean getSedativos() {
        return sedativos;
    }

    public void setSedativos(Boolean sedativos) {
        this.sedativos = sedativos;
    }

    public Boolean getAnsioliticos() {
        return ansioliticos;
    }

    public void setAnsioliticos(Boolean ansioliticos) {
        this.ansioliticos = ansioliticos;
    }

    public Boolean getHipnoticos() {
        return hipnoticos;
    }

    public void setHipnoticos(Boolean hipnoticos) {
        this.hipnoticos = hipnoticos;
    }

    public Boolean getFenotiazina() {
        return fenotiazina;
    }

    public void setFenotiazina(Boolean fenotiazina) {
        this.fenotiazina = fenotiazina;
    }

    public Boolean getAntiHipertensivos() {
        return antiHipertensivos;
    }

    public void setAntiHipertensivos(Boolean antiHipertensivos) {
        this.antiHipertensivos = antiHipertensivos;
    }

    public Boolean getAntiEpileticos() {
        return antiEpileticos;
    }

    public void setAntiEpileticos(Boolean antiEpileticos) {
        this.antiEpileticos = antiEpileticos;
    }

    public Boolean getConsumoAlcool() {
        return consumoAlcool;
    }

    public void setConsumoAlcool(Boolean consumoAlcool) {
        this.consumoAlcool = consumoAlcool;
    }

}
