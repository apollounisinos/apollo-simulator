package br.unisinos.apollo.model.bean;

import java.util.Date;

/**
 * Created 10/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com] =D
 */
public class EventoUsuario {

    private Integer eventoID;
    private Integer usuarioID;
    private Date datahora;
    private String tipo;
    private String movimentoUsuarioID;

    public Integer getEventoID() {
        return eventoID;
    }

    public void setEventoID(Integer eventoID) {
        this.eventoID = eventoID;
    }

    public Integer getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(Integer usuarioID) {
        this.usuarioID = usuarioID;
    }

    public Date getDatahora() {
        return datahora;
    }

    public void setDatahora(Date datahora) {
        this.datahora = datahora;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMovimentoUsuarioID() {
        return movimentoUsuarioID;
    }

    public void setMovimentoUsuarioID(String movimentoUsuarioID) {
        this.movimentoUsuarioID = movimentoUsuarioID;
    }

}
