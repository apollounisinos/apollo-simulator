package br.unisinos.apollo.model.bean;

/**
 * Created 08/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class Intrinseco {

    private Double imc;
    private Integer idade;
    private Double temporeacao;
    private Boolean antecedenteQueda;
    private Boolean deficitEquilibrio;
    private Boolean deficitVisual;
    private Boolean deficitMarcha;
    private Boolean demencia;
    private Boolean medoCair;
    private Boolean fraquezaMuscular;
    private Boolean deficienciaAuditiva;
    private Boolean sensibilidade;
    private Boolean deficienciaFisica;
    private Boolean osteoporose;
    private Boolean artrite;
    private Boolean doencaReumatica;
    private Boolean sincope;
    private Boolean lesaoSistemaNervosoCentral;
    private Boolean problemasNoPe;
    private Boolean depressao;
    private Boolean parkinson;
    private Boolean vertigem;
    private Boolean diabetes;
    private Boolean alteracaoPostural;
    private Boolean antecedenteAVC;
    private Boolean incontinenciaUrinaria;
    private Boolean comorbidade;
    private Boolean autoPercepcaoEstadoSaude;
    private Boolean dor;

    public Double getImc() {
        return imc;
    }

    public void setImc(Double imc) {
        this.imc = imc;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public Double getTemporeacao() {
        return temporeacao;
    }

    public void setTemporeacao(Double temporeacao) {
        this.temporeacao = temporeacao;
    }

    public Boolean getAntecedenteQueda() {
        return antecedenteQueda;
    }

    public void setAntecedenteQueda(Boolean antecedenteQueda) {
        this.antecedenteQueda = antecedenteQueda;
    }

    public Boolean getDeficitEquilibrio() {
        return deficitEquilibrio;
    }

    public void setDeficitEquilibrio(Boolean deficitEquilibrio) {
        this.deficitEquilibrio = deficitEquilibrio;
    }

    public Boolean getDeficitVisual() {
        return deficitVisual;
    }

    public void setDeficitVisual(Boolean deficitVisual) {
        this.deficitVisual = deficitVisual;
    }

    public Boolean getDeficitMarcha() {
        return deficitMarcha;
    }

    public void setDeficitMarcha(Boolean deficitMarcha) {
        this.deficitMarcha = deficitMarcha;
    }

    public Boolean getDemencia() {
        return demencia;
    }

    public void setDemencia(Boolean demencia) {
        this.demencia = demencia;
    }

    public Boolean getMedoCair() {
        return medoCair;
    }

    public void setMedoCair(Boolean medoCair) {
        this.medoCair = medoCair;
    }

    public Boolean getFraquezaMuscular() {
        return fraquezaMuscular;
    }

    public void setFraquezaMuscular(Boolean fraquezaMuscular) {
        this.fraquezaMuscular = fraquezaMuscular;
    }

    public Boolean getDeficienciaAuditiva() {
        return deficienciaAuditiva;
    }

    public void setDeficienciaAuditiva(Boolean deficienciaAuditiva) {
        this.deficienciaAuditiva = deficienciaAuditiva;
    }

    public Boolean getSensibilidade() {
        return sensibilidade;
    }

    public void setSensibilidade(Boolean sensibilidade) {
        this.sensibilidade = sensibilidade;
    }

    public Boolean getDeficienciaFisica() {
        return deficienciaFisica;
    }

    public void setDeficienciaFisica(Boolean deficienciaFisica) {
        this.deficienciaFisica = deficienciaFisica;
    }

    public Boolean getOsteoporose() {
        return osteoporose;
    }

    public void setOsteoporose(Boolean osteoporose) {
        this.osteoporose = osteoporose;
    }

    public Boolean getArtrite() {
        return artrite;
    }

    public void setArtrite(Boolean artrite) {
        this.artrite = artrite;
    }

    public Boolean getDoencaReumatica() {
        return doencaReumatica;
    }

    public void setDoencaReumatica(Boolean doencaReumatica) {
        this.doencaReumatica = doencaReumatica;
    }

    public Boolean getSincope() {
        return sincope;
    }

    public void setSincope(Boolean sincope) {
        this.sincope = sincope;
    }

    public Boolean getLesaoSistemaNervosoCentral() {
        return lesaoSistemaNervosoCentral;
    }

    public void setLesaoSistemaNervosoCentral(Boolean lesaoSistemaNervosoCentral) {
        this.lesaoSistemaNervosoCentral = lesaoSistemaNervosoCentral;
    }

    public Boolean getProblemasNoPe() {
        return problemasNoPe;
    }

    public void setProblemasNoPe(Boolean problemasNoPe) {
        this.problemasNoPe = problemasNoPe;
    }

    public Boolean getDepressao() {
        return depressao;
    }

    public void setDepressao(Boolean depressao) {
        this.depressao = depressao;
    }

    public Boolean getParkinson() {
        return parkinson;
    }

    public void setParkinson(Boolean parkinson) {
        this.parkinson = parkinson;
    }

    public Boolean getVertigem() {
        return vertigem;
    }

    public void setVertigem(Boolean vertigem) {
        this.vertigem = vertigem;
    }

    public Boolean getDiabetes() {
        return diabetes;
    }

    public void setDiabetes(Boolean diabetes) {
        this.diabetes = diabetes;
    }

    public Boolean getAlteracaoPostural() {
        return alteracaoPostural;
    }

    public void setAlteracaoPostural(Boolean alteracaoPostural) {
        this.alteracaoPostural = alteracaoPostural;
    }

    public Boolean getAntecedenteAVC() {
        return antecedenteAVC;
    }

    public void setAntecedenteAVC(Boolean antecedenteAVC) {
        this.antecedenteAVC = antecedenteAVC;
    }

    public Boolean getIncontinenciaUrinaria() {
        return incontinenciaUrinaria;
    }

    public void setIncontinenciaUrinaria(Boolean incontinenciaUrinaria) {
        this.incontinenciaUrinaria = incontinenciaUrinaria;
    }

    public Boolean getComorbidade() {
        return comorbidade;
    }

    public void setComorbidade(Boolean comorbidade) {
        this.comorbidade = comorbidade;
    }

    public Boolean getAutoPercepcaoEstadoSaude() {
        return autoPercepcaoEstadoSaude;
    }

    public void setAutoPercepcaoEstadoSaude(Boolean autoPercepcaoEstadoSaude) {
        this.autoPercepcaoEstadoSaude = autoPercepcaoEstadoSaude;
    }

    public Boolean getDor() {
        return dor;
    }

    public void setDor(Boolean dor) {
        this.dor = dor;
    }

}
