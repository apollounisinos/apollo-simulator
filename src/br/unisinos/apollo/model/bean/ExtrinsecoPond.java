package br.unisinos.apollo.model.bean;

/**
 * Created 05/01/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class ExtrinsecoPond {

    private Integer compromentimentoMobilidade;
    private Integer riscoAmbiental;
    private Integer internLongaPermanencia;
    private Integer viveSozinho;
    private Integer reducaoAtividadesBasicas;
    private Integer capacidadeManterseEmPe;
    private Integer capacidadeTransferirse;
    private Integer capacidadeDeitarLevantar;
    private Integer dependenciaFuncional;
    private Integer sedentarismo;
    private Integer polifarmacia;
    private Integer usoDrogasPsicoativas;
    private Integer benzoDiazepinicos;
    private Integer antiPsicoticos;
    private Integer antiDepressivos;
    private Integer sedativos;
    private Integer ansioliticos;
    private Integer hipnoticos;
    private Integer fenotiazina;
    private Integer antiHipertensivos;
    private Integer antiEpileticos;
    private Integer consumoAlcool;

    public Integer getCompromentimentoMobilidade() {
        return compromentimentoMobilidade;
    }

    public void setCompromentimentoMobilidade(Integer compromentimentoMobilidade) {
        this.compromentimentoMobilidade = compromentimentoMobilidade;
    }

    public Integer getRiscoAmbiental() {
        return riscoAmbiental;
    }

    public void setRiscoAmbiental(Integer riscoAmbiental) {
        this.riscoAmbiental = riscoAmbiental;
    }

    public Integer getInternLongaPermanencia() {
        return internLongaPermanencia;
    }

    public void setInternLongaPermanencia(Integer internLongaPermanencia) {
        this.internLongaPermanencia = internLongaPermanencia;
    }

    public Integer getViveSozinho() {
        return viveSozinho;
    }

    public void setViveSozinho(Integer viveSozinho) {
        this.viveSozinho = viveSozinho;
    }

    public Integer getReducaoAtividadesBasicas() {
        return reducaoAtividadesBasicas;
    }

    public void setReducaoAtividadesBasicas(Integer reducaoAtividadesBasicas) {
        this.reducaoAtividadesBasicas = reducaoAtividadesBasicas;
    }

    public Integer getCapacidadeManterseEmPe() {
        return capacidadeManterseEmPe;
    }

    public void setCapacidadeManterseEmPe(Integer capacidadeManterseEmPe) {
        this.capacidadeManterseEmPe = capacidadeManterseEmPe;
    }

    public Integer getCapacidadeTransferirse() {
        return capacidadeTransferirse;
    }

    public void setCapacidadeTransferirse(Integer capacidadeTransferirse) {
        this.capacidadeTransferirse = capacidadeTransferirse;
    }

    public Integer getCapacidadeDeitarLevantar() {
        return capacidadeDeitarLevantar;
    }

    public void setCapacidadeDeitarLevantar(Integer capacidadeDeitarLevantar) {
        this.capacidadeDeitarLevantar = capacidadeDeitarLevantar;
    }

    public Integer getDependenciaFuncional() {
        return dependenciaFuncional;
    }

    public void setDependenciaFuncional(Integer dependenciaFuncional) {
        this.dependenciaFuncional = dependenciaFuncional;
    }

    public Integer getSedentarismo() {
        return sedentarismo;
    }

    public void setSedentarismo(Integer sedentarismo) {
        this.sedentarismo = sedentarismo;
    }

    public Integer getPolifarmacia() {
        return polifarmacia;
    }

    public void setPolifarmacia(Integer polifarmacia) {
        this.polifarmacia = polifarmacia;
    }

    public Integer getUsoDrogasPsicoativas() {
        return usoDrogasPsicoativas;
    }

    public void setUsoDrogasPsicoativas(Integer usoDrogasPsicoativas) {
        this.usoDrogasPsicoativas = usoDrogasPsicoativas;
    }

    public Integer getBenzoDiazepinicos() {
        return benzoDiazepinicos;
    }

    public void setBenzoDiazepinicos(Integer benzoDiazepinicos) {
        this.benzoDiazepinicos = benzoDiazepinicos;
    }

    public Integer getAntiPsicoticos() {
        return antiPsicoticos;
    }

    public void setAntiPsicoticos(Integer antiPsicoticos) {
        this.antiPsicoticos = antiPsicoticos;
    }

    public Integer getAntiDepressivos() {
        return antiDepressivos;
    }

    public void setAntiDepressivos(Integer antiDepressivos) {
        this.antiDepressivos = antiDepressivos;
    }

    public Integer getSedativos() {
        return sedativos;
    }

    public void setSedativos(Integer sedativos) {
        this.sedativos = sedativos;
    }

    public Integer getAnsioliticos() {
        return ansioliticos;
    }

    public void setAnsioliticos(Integer ansioliticos) {
        this.ansioliticos = ansioliticos;
    }

    public Integer getHipnoticos() {
        return hipnoticos;
    }

    public void setHipnoticos(Integer hipnoticos) {
        this.hipnoticos = hipnoticos;
    }

    public Integer getFenotiazina() {
        return fenotiazina;
    }

    public void setFenotiazina(Integer fenotiazina) {
        this.fenotiazina = fenotiazina;
    }

    public Integer getAntiHipertensivos() {
        return antiHipertensivos;
    }

    public void setAntiHipertensivos(Integer antiHipertensivos) {
        this.antiHipertensivos = antiHipertensivos;
    }

    public Integer getAntiEpileticos() {
        return antiEpileticos;
    }

    public void setAntiEpileticos(Integer antiEpileticos) {
        this.antiEpileticos = antiEpileticos;
    }

    public Integer getConsumoAlcool() {
        return consumoAlcool;
    }

    public void setConsumoAlcool(Integer consumoAlcool) {
        this.consumoAlcool = consumoAlcool;
    }

    public Integer sum() {
        Integer sum = 0;
        sum += this.compromentimentoMobilidade;
        sum += this.riscoAmbiental;
        sum += this.internLongaPermanencia;
        sum += this.viveSozinho;
        sum += this.reducaoAtividadesBasicas;
        sum += this.capacidadeManterseEmPe;
        sum += this.capacidadeTransferirse;
        sum += this.capacidadeDeitarLevantar;
        sum += this.dependenciaFuncional;
        sum += this.sedentarismo;
        sum += this.polifarmacia;
        sum += this.usoDrogasPsicoativas;
        sum += this.benzoDiazepinicos;
        sum += this.antiPsicoticos;
        sum += this.antiDepressivos;
        sum += this.sedativos;
        sum += this.ansioliticos;
        sum += this.hipnoticos;
        sum += this.fenotiazina;
        sum += this.antiHipertensivos;
        sum += this.antiEpileticos;
        sum += this.consumoAlcool;
        return sum;
    }

}
