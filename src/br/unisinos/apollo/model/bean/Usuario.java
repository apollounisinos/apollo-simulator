package br.unisinos.apollo.model.bean;

import java.util.Date;

/**
 * Created 05/05/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class Usuario {

    private Integer usuarioID;
    private String nome;
    private Boolean historicoQueda;
    private Double altura;
    private Double peso;
    private String email;
    private Date dataNascimento;
    private String sexo;

    public Integer getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(Integer usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getHistoricoQueda() {
        return historicoQueda;
    }

    public void setHistoricoQueda(Boolean historicoQueda) {
        this.historicoQueda = historicoQueda;
    }

    public Double getAltura() {
        return altura;
    }

    public void setAltura(Double altura) {
        this.altura = altura;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

}
