package br.unisinos.apollo.model.bean;

/**
 * Created 05/01/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class IntrinsecoPond {

    private Integer antecedenteQueda;
    private Integer deficitEquilibrio;
    private Integer deficitVisual;
    private Integer deficitMarcha;
    private Integer demencia;
    private Integer medoCair;
    private Integer fraquezaMuscular;
    private Integer deficienciaAuditiva;
    private Integer sensibilidade;
    private Integer deficienciaFisica;
    private Integer osteoporose;
    private Integer artrite;
    private Integer doencaReumatica;
    private Integer sincope;
    private Integer lesaoSistemaNervosoCentral;
    private Integer problemasNoPe;
    private Integer depressao;
    private Integer parkinson;
    private Integer vertigem;
    private Integer diabetes;
    private Integer alteracaoPostural;
    private Integer antecedenteAVC;
    private Integer incontinenciaUrinaria;
    private Integer comorbidade;
    private Integer autoPercepcaoEstadoSaude;
    private Integer dor;

    public Integer getAntecedenteQueda() {
        return antecedenteQueda;
    }

    public void setAntecedenteQueda(Integer antecedenteQueda) {
        this.antecedenteQueda = antecedenteQueda;
    }

    public Integer getDeficitEquilibrio() {
        return deficitEquilibrio;
    }

    public void setDeficitEquilibrio(Integer deficitEquilibrio) {
        this.deficitEquilibrio = deficitEquilibrio;
    }

    public Integer getDeficitVisual() {
        return deficitVisual;
    }

    public void setDeficitVisual(Integer deficitVisual) {
        this.deficitVisual = deficitVisual;
    }

    public Integer getDeficitMarcha() {
        return deficitMarcha;
    }

    public void setDeficitMarcha(Integer deficitMarcha) {
        this.deficitMarcha = deficitMarcha;
    }

    public Integer getDemencia() {
        return demencia;
    }

    public void setDemencia(Integer demencia) {
        this.demencia = demencia;
    }

    public Integer getMedoCair() {
        return medoCair;
    }

    public void setMedoCair(Integer medoCair) {
        this.medoCair = medoCair;
    }

    public Integer getFraquezaMuscular() {
        return fraquezaMuscular;
    }

    public void setFraquezaMuscular(Integer fraquezaMuscular) {
        this.fraquezaMuscular = fraquezaMuscular;
    }

    public Integer getDeficienciaAuditiva() {
        return deficienciaAuditiva;
    }

    public void setDeficienciaAuditiva(Integer deficienciaAuditiva) {
        this.deficienciaAuditiva = deficienciaAuditiva;
    }

    public Integer getSensibilidade() {
        return sensibilidade;
    }

    public void setSensibilidade(Integer sensibilidade) {
        this.sensibilidade = sensibilidade;
    }

    public Integer getDeficienciaFisica() {
        return deficienciaFisica;
    }

    public void setDeficienciaFisica(Integer deficienciaFisica) {
        this.deficienciaFisica = deficienciaFisica;
    }

    public Integer getOsteoporose() {
        return osteoporose;
    }

    public void setOsteoporose(Integer osteoporose) {
        this.osteoporose = osteoporose;
    }

    public Integer getArtrite() {
        return artrite;
    }

    public void setArtrite(Integer artrite) {
        this.artrite = artrite;
    }

    public Integer getDoencaReumatica() {
        return doencaReumatica;
    }

    public void setDoencaReumatica(Integer doencaReumatica) {
        this.doencaReumatica = doencaReumatica;
    }

    public Integer getSincope() {
        return sincope;
    }

    public void setSincope(Integer sincope) {
        this.sincope = sincope;
    }

    public Integer getLesaoSistemaNervosoCentral() {
        return lesaoSistemaNervosoCentral;
    }

    public void setLesaoSistemaNervosoCentral(Integer lesaoSistemaNervosoCentral) {
        this.lesaoSistemaNervosoCentral = lesaoSistemaNervosoCentral;
    }

    public Integer getProblemasNoPe() {
        return problemasNoPe;
    }

    public void setProblemasNoPe(Integer problemasNoPe) {
        this.problemasNoPe = problemasNoPe;
    }

    public Integer getDepressao() {
        return depressao;
    }

    public void setDepressao(Integer depressao) {
        this.depressao = depressao;
    }

    public Integer getParkinson() {
        return parkinson;
    }

    public void setParkinson(Integer parkinson) {
        this.parkinson = parkinson;
    }

    public Integer getVertigem() {
        return vertigem;
    }

    public void setVertigem(Integer vertigem) {
        this.vertigem = vertigem;
    }

    public Integer getDiabetes() {
        return diabetes;
    }

    public void setDiabetes(Integer diabetes) {
        this.diabetes = diabetes;
    }

    public Integer getAlteracaoPostural() {
        return alteracaoPostural;
    }

    public void setAlteracaoPostural(Integer alteracaoPostural) {
        this.alteracaoPostural = alteracaoPostural;
    }

    public Integer getAntecedenteAVC() {
        return antecedenteAVC;
    }

    public void setAntecedenteAVC(Integer antecedenteAVC) {
        this.antecedenteAVC = antecedenteAVC;
    }

    public Integer getIncontinenciaUrinaria() {
        return incontinenciaUrinaria;
    }

    public void setIncontinenciaUrinaria(Integer incontinenciaUrinaria) {
        this.incontinenciaUrinaria = incontinenciaUrinaria;
    }

    public Integer getComorbidade() {
        return comorbidade;
    }

    public void setComorbidade(Integer comorbidade) {
        this.comorbidade = comorbidade;
    }

    public Integer getAutoPercepcaoEstadoSaude() {
        return autoPercepcaoEstadoSaude;
    }

    public void setAutoPercepcaoEstadoSaude(Integer autoPercepcaoEstadoSaude) {
        this.autoPercepcaoEstadoSaude = autoPercepcaoEstadoSaude;
    }

    public Integer getDor() {
        return dor;
    }

    public void setDor(Integer dor) {
        this.dor = dor;
    }

    public int sum() {
        int sum = 0;
        sum += this.antecedenteQueda;
        sum += this.deficitEquilibrio;
        sum += this.deficitVisual;
        sum += this.deficitMarcha;
        sum += this.demencia;
        sum += this.medoCair;
        sum += this.fraquezaMuscular;
        sum += this.deficienciaAuditiva;
        sum += this.sensibilidade;
        sum += this.deficienciaFisica;
        sum += this.osteoporose;
        sum += this.artrite;
        sum += this.doencaReumatica;
        sum += this.sincope;
        sum += this.lesaoSistemaNervosoCentral;
        sum += this.problemasNoPe;
        sum += this.depressao;
        sum += this.parkinson;
        sum += this.vertigem;
        sum += this.diabetes;
        sum += this.alteracaoPostural;
        sum += this.antecedenteAVC;
        sum += this.incontinenciaUrinaria;
        sum += this.comorbidade;
        sum += this.autoPercepcaoEstadoSaude;
        sum += this.dor;
        return sum;
    }

}
