package br.unisinos.apollo.model.bean;

/**
 * Created 22/01/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class FatorPond {

    private Double fu;
    private Double fa;
    private Double fi;
    private Double fe;
    private Double total;

    public Double getFu() {
        return fu;
    }

    public void setFu(Double fu) {
        this.fu = fu;
    }

    public Double getFa() {
        return fa;
    }

    public void setFa(Double fa) {
        this.fa = fa;
    }

    public Double getFi() {
        return fi;
    }

    public void setFi(Double fi) {
        this.fi = fi;
    }

    public Double getFe() {
        return fe;
    }

    public void setFe(Double fe) {
        this.fe = fe;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

}
