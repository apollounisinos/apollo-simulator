package br.unisinos.apollo.model.dao;

import br.unisinos.apollo.model.bean.Extrinseco;
import br.unisinos.apollo.model.bean.ExtrinsecoPond;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created 09/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class ExtrinsecoDAO {

    public Extrinseco getExtrinseco(Connection connection, Integer usuarioID) {
        PreparedStatement pstmt;
        Extrinseco extrinseco = new Extrinseco();
        try {
            String sql = "SELECT * FROM extrinseco WHERE usuario_id = ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, usuarioID);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                extrinseco.setCompromentimentoMobilidade(rs.getBoolean("comprometimentoMobilidade"));
                extrinseco.setRiscoAmbiental(rs.getBoolean("riscoAmbiental"));
                extrinseco.setInternLongaPermanencia(rs.getBoolean("internLongaPermanencia"));
                extrinseco.setViveSozinho(rs.getBoolean("viveSozinho"));
                extrinseco.setReducaoAtividadesBasicas(rs.getBoolean("reducaoAtividadesBasicas"));
                extrinseco.setCapacidadeManterseEmPe(rs.getBoolean("capacidadeManterseEmPe"));
                extrinseco.setCapacidadeTransferirse(rs.getBoolean("capacidadeTransferirse"));
                extrinseco.setCapacidadeDeitarLevantar(rs.getBoolean("capacidadeDeitarLevantar"));
                extrinseco.setDependenciaFuncional(rs.getBoolean("dependenciaFuncional"));
                extrinseco.setSedentarismo(rs.getBoolean("sedentarismo"));
                extrinseco.setPolifarmacia(rs.getBoolean("polifarmacia"));
                extrinseco.setUsoDrogasPsicoativas(rs.getBoolean("usoDrogasPsicoativas"));
                extrinseco.setBenzoDiazepinicos(rs.getBoolean("benzoDiazepinicos"));
                extrinseco.setAntiPsicoticos(rs.getBoolean("antiPsicoticos"));
                extrinseco.setAntiDepressivos(rs.getBoolean("antiDepressivos"));
                extrinseco.setSedativos(rs.getBoolean("sedativos"));
                extrinseco.setAnsioliticos(rs.getBoolean("ansioliticos"));
                extrinseco.setHipnoticos(rs.getBoolean("hipnoticos"));
                extrinseco.setFenotiazina(rs.getBoolean("fenotiazina"));
                extrinseco.setAntiHipertensivos(rs.getBoolean("antiHipertensivos"));
                extrinseco.setAntiEpileticos(rs.getBoolean("antiEpileticos"));
                extrinseco.setConsumoAlcool(rs.getBoolean("consumoAlcool"));
            }
        } catch (Exception e) {
            System.out.println("#Error: Return the extrinseco. Message: " + e.getMessage());
        }
        return extrinseco;
    }

    public ExtrinsecoPond getExtrinsecoPond(Connection connection) {
        PreparedStatement pstmt;
        ExtrinsecoPond extrinsecoPond = new ExtrinsecoPond();
        try {
            String sql = "SELECT * FROM extrinseco_pond WHERE extrinseco_pond_id = 1;";
            pstmt = connection.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                extrinsecoPond.setCompromentimentoMobilidade(rs.getInt("comprometimentoMobilidade_pond"));
                extrinsecoPond.setRiscoAmbiental(rs.getInt("riscoAmbiental_pond"));
                extrinsecoPond.setInternLongaPermanencia(rs.getInt("internLongaPermanencia_pond"));
                extrinsecoPond.setViveSozinho(rs.getInt("viveSozinho_pond"));
                extrinsecoPond.setReducaoAtividadesBasicas(rs.getInt("reducaoAtividadesBasicas_pond"));
                extrinsecoPond.setCapacidadeManterseEmPe(rs.getInt("capacidadeManterseEmPe_pond"));
                extrinsecoPond.setCapacidadeTransferirse(rs.getInt("capacidadeTransferirse_pond"));
                extrinsecoPond.setCapacidadeDeitarLevantar(rs.getInt("capacidadeDeitarLevantar_pond"));
                extrinsecoPond.setDependenciaFuncional(rs.getInt("dependenciaFuncional_pond"));
                extrinsecoPond.setSedentarismo(rs.getInt("sedentarismo_pond"));
                extrinsecoPond.setPolifarmacia(rs.getInt("polifarmacia_pond"));
                extrinsecoPond.setUsoDrogasPsicoativas(rs.getInt("usoDrogasPsicoativas_pond"));
                extrinsecoPond.setBenzoDiazepinicos(rs.getInt("benzoDiazepinicos_pond"));
                extrinsecoPond.setAntiPsicoticos(rs.getInt("antiPsicoticos_pond"));
                extrinsecoPond.setAntiDepressivos(rs.getInt("antiDepressivos_pond"));
                extrinsecoPond.setSedativos(rs.getInt("sedativos_pond"));
                extrinsecoPond.setAnsioliticos(rs.getInt("ansioliticos_pond"));
                extrinsecoPond.setHipnoticos(rs.getInt("hipnoticos_pond"));
                extrinsecoPond.setFenotiazina(rs.getInt("fenotiazina_pond"));
                extrinsecoPond.setAntiHipertensivos(rs.getInt("antiHipertensivos_pond"));
                extrinsecoPond.setAntiEpileticos(rs.getInt("antiEpileticos_pond"));
                extrinsecoPond.setConsumoAlcool(rs.getInt("consumoAlcool_pond"));
            }
        } catch (Exception e) {
            System.out.println("#Error: Retornar extrinseco pond. Mensagem: " + e.getMessage());
        }
        return extrinsecoPond;
    }

}
