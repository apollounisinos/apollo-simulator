package br.unisinos.apollo.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created 10/01/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class LuzPondDAO {

    public Integer getStatus(Connection connection, Integer luz) {
        PreparedStatement pstmt;
        Integer pond = 0;
        try {
            String sql = "SELECT status FROM luz_pond WHERE inf < ? AND sup > ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, luz);
            pstmt.setInt(2, luz);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                pond = rs.getInt("status");
            }
        } catch (Exception e) {
            System.out.println("#Error: Retornar a luz status. Mensagem: " + e.getMessage());
        }
        return pond;
    }

    public Integer getPond(Connection connection, Integer luz) {
        PreparedStatement pstmt;
        Integer pond = 0;
        try {
            String sql = "SELECT pond FROM luz_pond WHERE inf < ? AND sup > ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, luz);
            pstmt.setInt(2, luz);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                pond = rs.getInt("pond");
            }
        } catch (Exception e) {
            System.out.println("#Error: Retornar a luz pond. Mensagem: " + e.getMessage());
        }
        return pond;
    }

}
