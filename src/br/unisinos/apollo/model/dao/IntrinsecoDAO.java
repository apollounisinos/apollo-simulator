package br.unisinos.apollo.model.dao;

import br.unisinos.apollo.model.bean.Intrinseco;
import br.unisinos.apollo.model.bean.IntrinsecoPond;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created 09/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class IntrinsecoDAO {

    public Intrinseco getIntrinseco(Connection connection, Integer usuarioID) {
        PreparedStatement pstmt;
        Intrinseco intrinseco = new Intrinseco();
        try {
            String sql = "SELECT * FROM intrinseco WHERE usuario_id = ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, usuarioID);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                intrinseco.setImc(rs.getDouble("imc"));
                intrinseco.setIdade(rs.getInt("idade"));
                intrinseco.setTemporeacao(rs.getDouble("temporeacao"));
                intrinseco.setAntecedenteQueda(rs.getBoolean("antecedentedequeda"));
                intrinseco.setDeficitEquilibrio(rs.getBoolean("deficitequilibrio"));
                intrinseco.setDeficitVisual(rs.getBoolean("deficitvisual"));
                intrinseco.setDeficitMarcha(rs.getBoolean("deficitmarcha"));
                intrinseco.setDemencia(rs.getBoolean("demencia"));
                intrinseco.setMedoCair(rs.getBoolean("medocair"));
                intrinseco.setFraquezaMuscular(rs.getBoolean("fraquezamuscular"));
                intrinseco.setDeficienciaAuditiva(rs.getBoolean("deficienciaauditiva"));
                intrinseco.setSensibilidade(rs.getBoolean("sensibilidade"));
                intrinseco.setDeficienciaFisica(rs.getBoolean("deficienciaFisica"));
                intrinseco.setOsteoporose(rs.getBoolean("osteoporose"));
                intrinseco.setArtrite(rs.getBoolean("artrite"));
                intrinseco.setDoencaReumatica(rs.getBoolean("doencaReumatica"));
                intrinseco.setSincope(rs.getBoolean("sincope"));
                intrinseco.setLesaoSistemaNervosoCentral(rs.getBoolean("lesaoSistemaNervosoCentral"));
                intrinseco.setProblemasNoPe(rs.getBoolean("problemasNoPe"));
                intrinseco.setDepressao(rs.getBoolean("depressao"));
                intrinseco.setParkinson(rs.getBoolean("parkinson"));
                intrinseco.setVertigem(rs.getBoolean("vertigem"));
                intrinseco.setDiabetes(rs.getBoolean("diabetes"));
                intrinseco.setAlteracaoPostural(rs.getBoolean("alteracaoPostural"));
                intrinseco.setAntecedenteAVC(rs.getBoolean("antecedente_avc"));
                intrinseco.setIncontinenciaUrinaria(rs.getBoolean("incontinenciaUrinaria"));
                intrinseco.setComorbidade(rs.getBoolean("comorbidade"));
                intrinseco.setAutoPercepcaoEstadoSaude(rs.getBoolean("autoPercepcaoEstadoSaude"));
                intrinseco.setDor(rs.getBoolean("dor"));
            }
        } catch (Exception e) {
            System.out.println("#Error: Retornar intrinseco. Message: " + e.getMessage());
        }
        return intrinseco;
    }

    public IntrinsecoPond getIntrinsecoPond(Connection connection) {
        PreparedStatement pstmt;
        IntrinsecoPond intrinsecoPeso = new IntrinsecoPond();
        try {
            String sql = "SELECT * FROM intrinseco_pond WHERE intrinseco_pond_id = 1;";
            pstmt = connection.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                intrinsecoPeso.setAntecedenteQueda(rs.getInt("antecedentequeda_pond"));
                intrinsecoPeso.setDeficitEquilibrio(rs.getInt("deficitequilibrio_pond"));
                intrinsecoPeso.setDeficitVisual(rs.getInt("deficitvisual_pond"));
                intrinsecoPeso.setDeficitMarcha(rs.getInt("deficitmarcha_pond"));
                intrinsecoPeso.setDemencia(rs.getInt("demencia_pond"));
                intrinsecoPeso.setMedoCair(rs.getInt("medocair_pond"));
                intrinsecoPeso.setFraquezaMuscular(rs.getInt("fraquezamuscular_pond"));
                intrinsecoPeso.setDeficienciaAuditiva(rs.getInt("deficienciaauditiva_pond"));
                intrinsecoPeso.setSensibilidade(rs.getInt("sensibilidade_pond"));
                intrinsecoPeso.setDeficienciaFisica(rs.getInt("deficienciaFisica_pond"));
                intrinsecoPeso.setOsteoporose(rs.getInt("osteoporose_pond"));
                intrinsecoPeso.setArtrite(rs.getInt("artrite_pond"));
                intrinsecoPeso.setDoencaReumatica(rs.getInt("doencaReumatica_pond"));
                intrinsecoPeso.setSincope(rs.getInt("sincope_pond"));
                intrinsecoPeso.setLesaoSistemaNervosoCentral(rs.getInt("lesaoSistemaNervosoCentral_pond"));
                intrinsecoPeso.setProblemasNoPe(rs.getInt("problemasNoPe_pond"));
                intrinsecoPeso.setDepressao(rs.getInt("depressao_pond"));
                intrinsecoPeso.setParkinson(rs.getInt("parkinson_pond"));
                intrinsecoPeso.setVertigem(rs.getInt("vertigem_pond"));
                intrinsecoPeso.setDiabetes(rs.getInt("diabetes_pond"));
                intrinsecoPeso.setAlteracaoPostural(rs.getInt("alteracaoPostural_pond"));
                intrinsecoPeso.setAntecedenteAVC(rs.getInt("antecedente_avc_pond"));
                intrinsecoPeso.setIncontinenciaUrinaria(rs.getInt("incontinenciaUrinaria_pond"));
                intrinsecoPeso.setComorbidade(rs.getInt("comorbidade_pond"));
                intrinsecoPeso.setAutoPercepcaoEstadoSaude(rs.getInt("autoPercepcaoEstadoSaude_pond"));
                intrinsecoPeso.setDor(rs.getInt("dor_pond"));
            }
        } catch (Exception e) {
            System.out.println("#Error: Retornar intrinseco pond. Mensagem: " + e.getMessage());
        }
        return intrinsecoPeso;
    }

}
