package br.unisinos.apollo.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created 10/01/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class IdadePondDAO {

    public Integer getPond(Connection connection, Integer idade) {
        PreparedStatement pstmt;
        Integer pond = 0;
        try {
            String sql = "SELECT pond FROM idade_pond WHERE inf < ? AND sup > ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, idade);
            pstmt.setInt(2, idade);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                pond = rs.getInt("pond");
            }
        } catch (Exception e) {
            System.out.println("#Error: Retornar idade pond. Message: " + e.getMessage());
        }
        return pond;
    }

}
