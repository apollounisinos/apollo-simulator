package br.unisinos.apollo.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created 30/01/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class SexoPondDAO {

    public Integer getPond(Connection connection, String sexo) {
        PreparedStatement pstmt;
        Integer pond = 0;
        try {
            String sql = "SELECT pond FROM sexo_pond WHERE sexo = ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, sexo);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                pond = rs.getInt("pond");
            }
        } catch (Exception e) {
            System.out.println("#Error: Retornar sexo pond. Mensagem: " + e.getMessage());
        }
        return pond;
    }

}
