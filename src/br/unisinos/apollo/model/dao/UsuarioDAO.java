package br.unisinos.apollo.model.dao;

import br.unisinos.apollo.model.bean.Usuario;
import br.unisinos.apollo.model.util.DateUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created 28/08/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com] =D
 */
public class UsuarioDAO {

    public Usuario getInformacaoUsuario(Connection connection, Integer usuarioID) {
        PreparedStatement pstmt;
        Usuario usuario = new Usuario();
        try {
            String sql = "SELECT * FROM usuario WHERE usuario_id = ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, usuarioID);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                usuario.setUsuarioID(rs.getInt("usuario_id"));
                usuario.setNome(rs.getString("nome"));
                usuario.setPeso(rs.getDouble("peso"));
                usuario.setSexo(rs.getString("sexo"));
                usuario.setEmail(rs.getString("emailemergencia"));
                usuario.setAltura(rs.getDouble("altura"));
                usuario.setDataNascimento(new DateUtil().stringTimestampToJavaDate2(rs.getString("datanascimento")));
            }
        } catch (Exception e) {
            System.out.println("#Error: Retornar informação do usuário. Message: " + e.getMessage());
        }
        return usuario;
    }

}
