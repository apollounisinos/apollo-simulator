package br.unisinos.apollo.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created 10/01/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class IMCPondDAO {

    public Integer getPond(Connection connection, Double imc) {
        PreparedStatement pstmt;
        Integer pond = 0;
        try {
            String sql = "SELECT pond FROM imc_pond WHERE inf < ? AND sup > ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setDouble(1, imc);
            pstmt.setDouble(2, imc);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                pond = rs.getInt("pond");
            }
        } catch (Exception e) {
            System.out.println("#Error: Return the pond of IMC. Message: " + e.getMessage());
        }
        return pond;
    }

}
