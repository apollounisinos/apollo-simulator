package br.unisinos.apollo.model.dao;

import br.unisinos.apollo.model.bean.FatorPond;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created 22/01/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class GeralDAO {

    public void limparEvento(Connection connection) {
        PreparedStatement pstmt;
        try {
            String sql = "DELETE FROM evento;";
            pstmt = connection.prepareStatement(sql);
            pstmt.executeUpdate();
        } catch (Exception e) {
            System.out.println("#Error: Falha ao limpar tabela de evento. Message: " + e.getMessage());
        }
    }

    public FatorPond getFatorPond(Connection connection) {
        PreparedStatement pstmt;
        FatorPond pond = new FatorPond();
        try {
            String sql = "SELECT * FROM fator_pond;";
            pstmt = connection.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                pond.setFu(rs.getDouble("fu"));
                pond.setFa(rs.getDouble("fa"));
                pond.setFi(rs.getDouble("fi"));
                pond.setFe(rs.getDouble("fe"));
                pond.setTotal(rs.getDouble("total"));
            }
        } catch (Exception e) {
            System.out.println("#Error: Retornar Fator Pond. Message: " + e.getMessage());
        }
        return pond;
    }

}
