package br.unisinos.apollo.model.dao;

import br.unisinos.apollo.model.bean.EventoUsuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created 10/11/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com] =D
 */
public class EventoDAO {

    private final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public void persistirEvento(Connection connection, EventoUsuario evento) {
        PreparedStatement pstmt;
        try {
            String sql = "INSERT INTO evento(usuario_id, datahora, tipo, movimentousuario_id) VALUES (?, ?, ?, ?);";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, evento.getUsuarioID());
            pstmt.setString(2, df.format(evento.getDatahora()));
            pstmt.setString(3, evento.getTipo() + "");
            pstmt.setString(4, evento.getMovimentoUsuarioID());
            pstmt.execute();
        } catch (Exception e) {
            System.out.println("#Error: Inserir novo evento. Mensagem: " + e.getMessage());
        }
    }

    public List<EventoUsuario> getEvento(Connection connection, Integer usuarioID) {
        PreparedStatement pstmt;
        List<EventoUsuario> eventos = new ArrayList<>();
        try {
            String sql = "SELECT * FROM evento WHERE usuario_id = ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, usuarioID);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                EventoUsuario event = new EventoUsuario();
                event.setEventoID(rs.getInt("evento_id"));
                event.setUsuarioID(rs.getInt("usuario_id"));
                event.setDatahora(df.parse(rs.getString("datahora")));
                event.setTipo(rs.getString("tipo"));
                event.setMovimentoUsuarioID(rs.getString("movimentousuario_id"));
                eventos.add(event);
            }
        } catch (Exception e) {
            System.out.println("#Error: Retornar evento. Mensagem: " + e.getMessage());
        }
        return eventos ;
    }

}
