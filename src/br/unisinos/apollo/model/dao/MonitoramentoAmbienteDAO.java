package br.unisinos.apollo.model.dao;

import br.unisinos.apollo.model.bean.MonitoramentoAmbiente;
import br.unisinos.apollo.model.util.DateUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Created 26/05/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com] =D
 */
public class MonitoramentoAmbienteDAO {

    public void persistMonitoramentoAmbiente(Connection connection, MonitoramentoAmbiente monAmbiente) {
        PreparedStatement pstmt;
        try {
            String sql = "INSERT INTO monitoramentoambiente(ambiente_id, "
                    + "temperatura, iluminacao, datahora) VALUES (?, ?, ?, ?);";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, monAmbiente.getAmbienteID());
            pstmt.setDouble(2, monAmbiente.getTemperatura());
            pstmt.setDouble(3, monAmbiente.getIluminacao());
            pstmt.setTimestamp(4, new DateUtil().dateToTimestamp(monAmbiente.getDatahora()));
            pstmt.execute();
        } catch (Exception e) {
            System.out.println("#Error: Inserir novo monitoramento ambiente. Mensagem: " + e.getMessage());
        }
    }

}
