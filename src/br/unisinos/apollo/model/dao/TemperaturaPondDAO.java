package br.unisinos.apollo.model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created 10/01/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class TemperaturaPondDAO {

    public Integer getPond(Connection connection, Integer temp) {
        PreparedStatement pstmt;
        Integer pond = 0;
        try {
            String sql = "SELECT pond FROM temperatura_pond WHERE inf < ? AND sup > ?;";
            pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, temp);
            pstmt.setInt(2, temp);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                pond = rs.getInt("pond");
            }
        } catch (Exception e) {
            System.out.println("#Error: Retornar a temperatura pond. Mensagem: " + e.getMessage());
        }
        return pond;
    }

}
