package br.unisinos.apollo.model.util;

import java.util.ArrayList;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;

/**
 * Validation class containing the methods to be used on sheets.
 *
 * @author Bruno Mota
 * @author Cristiano Farias
 */
public class ValidationUtil {

    private final WorkbookUtil wUtil = new WorkbookUtil();

    /**
     * Read and retrive a ArrayList containing the headers on String format.
     *
     * @param sheet to read the headers.
     * @param collIndex the index to start reading for.
     * @return ArrayList of String with the headers.
     */
    public ArrayList<String> readHeader(XSSFSheet sheet, Integer collIndex) {
        ArrayList<String> headers = new ArrayList<>();
        for (int r = 0; r < 1; r++) {
            Row row = sheet.getRow(r);
            for (int c = collIndex; c < row.getPhysicalNumberOfCells(); c++) {
                Cell cell = row.getCell(c);
                if (cell != null) {
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    headers.add(cell.getStringCellValue());
                }
            }
        }
        return headers;
    }

    /**
     * Compare the original headers with the result from readHeader. Return null
     * or the originals not found.
     *
     * @param original the headers that must compose the file.
     * @param fromXlsx the headers that really comprise the file.
     * @return ArrayList with the headers not found or null.
     */
    public ArrayList<String> compareHeader(ArrayList<String> original, ArrayList<String> fromXlsx) {
        for (int x = 0; x < fromXlsx.size(); x++) {
            for (int o = 0; o < original.size(); o++) {
                if (fromXlsx.get(x).equalsIgnoreCase(original.get(o))) {
                    original.remove(o);
                }
            }
        }
        return original;
    }

    /**
     * Receive two lists and return a list with the data out of order.
     *
     * @param original the headers that must compose the file.
     * @param fromXlsx the headers that really comprise the file.
     * @return Return the list with the headers out of order.
     */
    public ArrayList<String> compareOrder(ArrayList<String> original, ArrayList<String> fromXlsx) {
        ArrayList<String> outOfOrder = new ArrayList<>();
        for (int x = 0; x < original.size(); x++) {
            if (!original.get(x).equalsIgnoreCase(fromXlsx.get(x))) {
                outOfOrder.add(original.get(x));
            }
        }
        return outOfOrder;
    }

    /**
     * Receive a sheet and cross the rows until find the first equals to "".
     * Then retrieve the last filled cell index.
     *
     * @param sheet used to validate the lines.
     * @param column
     * @return Integer last filled cell/row index.
     */
    public Integer retrieveLastIndexRow(XSSFSheet sheet, int column) {
        int last = 0;
        try {
            for (int r = 1; r < sheet.getPhysicalNumberOfRows(); r++) {
                Row row = sheet.getRow(r);
                String cell = wUtil.readStringForValidation(row, column);
                if (cell.equalsIgnoreCase("")) {
                    last = r;
                    break;
                }
            }
            if (last == 0) {
                last = sheet.getPhysicalNumberOfRows();
            }
        } catch (Exception e) {
//            System.out.println("FALHA PARA REALIZAR BUSCA DE ULTIMA LINHA VALIDA NA PLANILHA (PLANILHA COM BUG)");
            e.printStackTrace();
        }
        return last;
    }

    /**
     * Receive a sheet and cross the rows until find the first equals to "".
     * Then retrieve the last filled cell index.
     *
     * @param sheet used to validate the lines.
     * @return Integer last filled cell/row index.
     */
    public Integer retrieveLastIndexRow(XSSFSheet sheet) {
        return retrieveLastIndexRow(sheet, 0);
    }

    /**
     * Receive a sheet and cross the cells on the first row until find the first
     * cell equals to "". Then retrieve the last filled cell/column index.
     *
     * @param sheet used to validate the columns.
     * @return Integer last filled cell/column index.
     */
    public Integer retrieveLastIndexColumn(XSSFSheet sheet) {
        int last = 0;
        try {
            Row row = sheet.getRow(0);
            for (int c = 0; c < row.getPhysicalNumberOfCells(); c++) {
                String cell = wUtil.readString(row, c);
                if (cell.equalsIgnoreCase("")) {
                    last = c - 1;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return last;
    }

    /**
     * Metodo By CenterTools.
     *
     * @param value valor passado.
     * @param values valores passados.
     * @return Boolean
     */
    public Boolean valuesSameSignificance(Object value, String... values) {
        for (String v : values) {
            if (value.toString().equals(v)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Computes the difference between two Double values returning TRUE when
     * within the established error margin, or false if it is outside.
     *
     * @param amount Value refers to the total.
     * @param valueCompare Value that will be subtracted from the total.
     * @param margin Margin of error that will be considered.
     * @param printError Defines whether when it fails to validate the
     * information must be shown.
     * @return valor TRUE ou FALSE referente ao resultado da comparação dos
     * valores e a margem de erro.
     */
    public Boolean compareDifferenceValues(double amount, double valueCompare, double margin, boolean printError) {
        double difference = amount - valueCompare;

        if (Math.abs(difference) <= margin) {
            return true;
        } else {
            if (printError) {
                System.out.println("");
                System.out.println("Valor 1: " + amount);
                System.out.println("Valor 2: " + valueCompare);
                System.out.println("Diferença: " + difference);
                System.out.println("Margem de Erro: " + margin);
                System.out.println("");
            }
            return false;
        }
    }

    /**
     * Computes the difference between two Double values returning TRUE when
     * within the established error margin, or false if it is outside.
     *
     * @param amount Value refers to the total.
     * @param valueCompare Value that will be subtracted from the total.
     * @param margin Margin of error that will be considered.
     * @return valor TRUE ou FALSE referente ao resultado da comparação dos
     * valores e a margem de erro.
     */
    public Boolean compareDifferenceValues(double amount, double valueCompare, double margin) {
        return compareDifferenceValues(amount, valueCompare, margin, false);
    }

    public Boolean validateString(String value) {
        if (value == null || value.isEmpty() || value.equalsIgnoreCase("") || value.equalsIgnoreCase(" ")) {
            return false;
        } else {
            return true;
        }
    }

}
