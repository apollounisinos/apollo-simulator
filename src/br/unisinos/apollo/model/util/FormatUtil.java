package br.unisinos.apollo.model.util;

import java.text.DecimalFormat;

/**
 * Created 29/01/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class FormatUtil {

    public String formatDoubleValue(Double value) {
        DecimalFormat df = new DecimalFormat("#.00");
        String dou = df.format(value);
        dou = dou.replace(",", ".");
        return dou;
    }

}
