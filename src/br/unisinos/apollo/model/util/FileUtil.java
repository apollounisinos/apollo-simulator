package br.unisinos.apollo.model.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created 05/02/2019
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class FileUtil {

    public void saveLog(String path, String newInfo, Date date, Boolean showDate) {
        try {
            //Date
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            File file = new File(path);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            if (showDate) {
                bw.write(df.format(date) + " | " + newInfo);
            } else {
                bw.write(newInfo);
            }
            bw.newLine();
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
