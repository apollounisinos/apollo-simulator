package br.unisinos.apollo.view;

import br.unisinos.apollo.controller.EventoController;
import br.unisinos.apollo.model.Analytics;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Created 10/12/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class Main extends javax.swing.JFrame {

    private JFileChooser choo = new JFileChooser();

    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();
    }

    static public void appendText(String text, Boolean newLine) {
        if (newLine) {
            Main.taOutput.setText(Main.taOutput.getText() + text + "\n");
        } else {
            Main.taOutput.setText(Main.taOutput.getText() + text);
        }
        Main.taOutput.setCaretPosition(taOutput.getText().length());
    }

    public void disableComponents() {
        tfDatasetUsuario.setEnabled(false);
        tfDatasetAmbiente.setEnabled(false);
        tfJump.setEnabled(false);
        tfAlerta.setEnabled(false);
        tfRisco.setEnabled(false);
        tfQueda.setEnabled(false);
        tfUsuario.setEnabled(false);
        btDatasetUsuario.setEnabled(false);
        btDatasetAmbiente.setEnabled(false);
        btExportarEvento.setEnabled(false);
        btProcessar.setEnabled(false);
    }

    public void enableComponents() {
        tfDatasetUsuario.setEnabled(true);
        tfDatasetAmbiente.setEnabled(true);
        tfJump.setEnabled(true);
        tfAlerta.setEnabled(true);
        tfRisco.setEnabled(true);
        tfQueda.setEnabled(true);
        tfUsuario.setEnabled(true);
        btDatasetUsuario.setEnabled(true);
        btDatasetAmbiente.setEnabled(true);
        btExportarEvento.setEnabled(true);
        btProcessar.setEnabled(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taOutput = new javax.swing.JTextArea();
        btProcessar = new javax.swing.JButton();
        lbOutput = new javax.swing.JLabel();
        lbDatasetUsuario = new javax.swing.JLabel();
        tfDatasetUsuario = new javax.swing.JTextField();
        btDatasetUsuario = new javax.swing.JButton();
        lbJump = new javax.swing.JLabel();
        lbUsuario = new javax.swing.JLabel();
        tfUsuario = new javax.swing.JTextField();
        lbAlerta = new javax.swing.JLabel();
        tfAlerta = new javax.swing.JTextField();
        lbRisco = new javax.swing.JLabel();
        tfRisco = new javax.swing.JTextField();
        lbQueda = new javax.swing.JLabel();
        tfQueda = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        btExportarEvento = new javax.swing.JButton();
        tfJump = new javax.swing.JTextField();
        lbDatasetAmbiente = new javax.swing.JLabel();
        tfDatasetAmbiente = new javax.swing.JTextField();
        btDatasetAmbiente = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Apollo Simulator");

        taOutput.setColumns(20);
        taOutput.setRows(5);
        jScrollPane1.setViewportView(taOutput);

        btProcessar.setText("Processar");
        btProcessar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btProcessarActionPerformed(evt);
            }
        });

        lbOutput.setText("Log output");

        lbDatasetUsuario.setText("Usuario: ");

        tfDatasetUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfDatasetUsuarioActionPerformed(evt);
            }
        });

        btDatasetUsuario.setText("...");
        btDatasetUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDatasetUsuarioActionPerformed(evt);
            }
        });

        lbJump.setText("Jump:");

        lbUsuario.setText("Usuário ID:");

        lbAlerta.setText("Alerta:");

        lbRisco.setText("Risco:");

        lbQueda.setText("Queda:");

        jLabel2.setText("v1.5  - 07 /02/2019");

        btExportarEvento.setText("Exportar Evento");
        btExportarEvento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btExportarEventoActionPerformed(evt);
            }
        });

        lbDatasetAmbiente.setText("Ambiente:");

        tfDatasetAmbiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfDatasetAmbienteActionPerformed(evt);
            }
        });

        btDatasetAmbiente.setText("...");
        btDatasetAmbiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDatasetAmbienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbDatasetUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbJump, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbUsuario)
                            .addComponent(lbRisco, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfDatasetUsuario)
                            .addComponent(tfJump)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tfRisco)
                                    .addComponent(tfUsuario))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbAlerta, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbQueda, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(9, 9, 9)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(tfAlerta)
                                    .addComponent(tfQueda))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btDatasetUsuario))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 621, Short.MAX_VALUE)
                        .addComponent(btExportarEvento)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btProcessar))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lbDatasetAmbiente, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(tfDatasetAmbiente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btDatasetAmbiente))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(lbOutput))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbDatasetUsuario)
                    .addComponent(btDatasetUsuario)
                    .addComponent(tfDatasetUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbDatasetAmbiente)
                    .addComponent(btDatasetAmbiente)
                    .addComponent(tfDatasetAmbiente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbJump)
                    .addComponent(tfJump, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbAlerta)
                    .addComponent(tfAlerta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbUsuario))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfRisco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfQueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbQueda)
                    .addComponent(lbRisco))
                .addGap(18, 18, 18)
                .addComponent(lbOutput)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btProcessar)
                    .addComponent(jLabel2)
                    .addComponent(btExportarEvento))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btDatasetUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDatasetUsuarioActionPerformed
        FileNameExtensionFilter fn = new FileNameExtensionFilter("xlsx", "xlsx");
        choo.setAcceptAllFileFilterUsed(false);
        choo.setFileFilter(fn);
        choo.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int res = choo.showOpenDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
            File file = choo.getSelectedFile();
            tfDatasetUsuario.setText(file.getPath());
        }
    }//GEN-LAST:event_btDatasetUsuarioActionPerformed

    private void tfDatasetUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfDatasetUsuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfDatasetUsuarioActionPerformed

    private void btProcessarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btProcessarActionPerformed
        taOutput.setText("");
        new Analytics().analysis(this, tfDatasetUsuario.getText(),
                tfDatasetAmbiente.getText(),
                Double.parseDouble(tfAlerta.getText()),
                Double.parseDouble(tfRisco.getText()),
                Double.parseDouble(tfQueda.getText()),
                Integer.parseInt(tfUsuario.getText()));
    }//GEN-LAST:event_btProcessarActionPerformed

    private void btExportarEventoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btExportarEventoActionPerformed
        new EventoController().exportarEvento(Integer.parseInt(tfUsuario.getText()));
        JOptionPane.showMessageDialog(null, "Evento exportado com sucesso!");
    }//GEN-LAST:event_btExportarEventoActionPerformed

    private void tfDatasetAmbienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfDatasetAmbienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfDatasetAmbienteActionPerformed

    private void btDatasetAmbienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDatasetAmbienteActionPerformed
        FileNameExtensionFilter fn = new FileNameExtensionFilter("xlsx", "xlsx");
        choo.setAcceptAllFileFilterUsed(false);
        choo.setFileFilter(fn);
        choo.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int res = choo.showOpenDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
            File file = choo.getSelectedFile();
            tfDatasetAmbiente.setText(file.getPath());
        }
    }//GEN-LAST:event_btDatasetAmbienteActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btDatasetAmbiente;
    private javax.swing.JButton btDatasetUsuario;
    private javax.swing.JButton btExportarEvento;
    private javax.swing.JButton btProcessar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbAlerta;
    private javax.swing.JLabel lbDatasetAmbiente;
    private javax.swing.JLabel lbDatasetUsuario;
    private javax.swing.JLabel lbJump;
    private javax.swing.JLabel lbOutput;
    private javax.swing.JLabel lbQueda;
    private javax.swing.JLabel lbRisco;
    private javax.swing.JLabel lbUsuario;
    public static javax.swing.JTextArea taOutput;
    private javax.swing.JTextField tfAlerta;
    private javax.swing.JTextField tfDatasetAmbiente;
    private javax.swing.JTextField tfDatasetUsuario;
    private javax.swing.JTextField tfJump;
    private javax.swing.JTextField tfQueda;
    private javax.swing.JTextField tfRisco;
    private javax.swing.JTextField tfUsuario;
    // End of variables declaration//GEN-END:variables
}
